#ifndef _MY_HEADER_H_
#define _MY_HEADER_H_

#define MAXMSGLEN 2048
#define INT_SIZE sizeof(int)
#define OPERATE_TYPE_SIZE 1

/* OPERATE_TYPE */
#define TYPE_OPEN 1
#define TYPE_CLOSE 2
#define TYPE_WRITE 3
#define TYPE_READ 4
#define TYPE_UNLINK 5
#define TYPE_LSEEK 6
#define TYPE_GETDIRENTRIES 7
#define TYPE_XSTAT 8
#define TYPE_GETDIRTREE 9

#endif
