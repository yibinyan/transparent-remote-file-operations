PROGS=server

all: mylib.so server

queue.o: queue.c
	gcc -Wall -fPIC -DPIC -c queue.c

socket_helper.o: socket_helper.c
	gcc -Wall -fPIC -DPIC -c socket_helper.c

mylib.o: mylib.c
	gcc -Wall -fPIC -DPIC -c mylib.c -I../include  -L../lib 

mylib.so: mylib.o queue.o socket_helper.o
	ld -shared -o mylib.so mylib.o queue.o socket_helper.o -ldl

server: server.c queue.o socket_helper.o
	gcc -Wall -o server queue.o socket_helper.o server.c -I../include -L../lib/ -lpthread -ldirtree

clean:
	rm -f *.o *.so $(PROGS)

