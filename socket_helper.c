#include <arpa/inet.h>
#include <sys/socket.h>
#include "myheader.h"
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <err.h>
#include <stdio.h>

void send_message(int sockfd, char *data, int length){
	
    int rv;

    rv = send(sockfd, data, length, 0);
    
    if (rv < 0){
        err(1, 0);
    }
}

int receive_message(int sockfd, char **buf){
	char temp[MAXMSGLEN]; 
    char *output = (char *)malloc(MAXMSGLEN);
    int output_size = MAXMSGLEN;
    int rest_len = output_size;
    int current_len = 0;
    int total_len = 0;
    int recv_len = MAXMSGLEN;
    int rv; 
    /* first 4 bytes indicate the length of response */
    int base_length = INT_SIZE; 
    
    /* get request from client */
    while ((rv = recv(sockfd, temp, recv_len, 0)) > 0) {
        
        if (rv > rest_len){
            //printf("EXTEND!");
            output_size = output_size + MAXMSGLEN;
            rest_len = rest_len + MAXMSGLEN;
            char *new_output;
            new_output = (char *)realloc(output, sizeof(char) * output_size);
            output = new_output;
        }
            
        memcpy(output + current_len, temp, rv);        
        rest_len = rest_len - rv;
        current_len = current_len + rv;    
        
        if (current_len  < base_length){
            /* still do not the total length of response */
        }else{
            /* get first 4 bytes */
            if (total_len == 0){
                total_len = *(int *)output;
            }
    
            if (current_len == total_len){
                /* have already received full data */
                break;
            }else{
                if (total_len - current_len <= MAXMSGLEN){
                    /* set recv_len to the length of rest data */
                    recv_len = total_len - current_len;
                }
            }
        }
    }


    
    if (rv<0) {
        err(1,0); // in case something went wrong
    }
   
    /* skip the length field */
    *buf = output + 4;
    return current_len;  
    //memcpy(buf, output + base_length, total_len - base_length);
}

void free_recv_buf(char *recvbuf){
    /* since we skip the length field 
     * in function recevie message, we need to free this part */
    free(recvbuf - 4);
}
