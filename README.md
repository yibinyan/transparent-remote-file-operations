# README #
## Project Overview ##

* Build an Remote Procedure Call (RPC) system to allow remote file operation (open, read, write, . . . ). 
* Designed and implemented a server process and a client stub library in C.

# Details
Main job of this project is to successfully pass arguments in to the server when client call stub library. After receiving arguments server will call corresponding function. Finally turn return value back to client. The most important part in this project is to design a reliable protocol to make sure arguments and return values can be transferred properly. Below is the format of the protocol I designed.        

**This is the format of message that client send to server:**

| TOTAL LENGTH 	| TYPE 	| ARGUMENTS ... 	| 

*TOTAL LENGTH(4 bytes)*:This field contains the total length of message. Receiver will block until receiving the first 4 bytes. Then it can count length of data it received. If it is less than 4 bytes, receiver will keep reading from socket.

*TYPE (1 bytes)*: Type defines which function server will call correspondingly. For example, server will call OPEN function when TYPE = 1. (All types is defined in myheader.h)

*ARGUMENTS*: Numbers and types of arguments may vary. But since server knows what which function it will call, it can process arguments field accordingly. Note that, stub functions whose arguments contains pointer (eg. buf in write()), data that pointer points to would be put into message rather than pointer itself.       

**This is the format of message that server will send back to client:**

| TOTAL LENGTH 	| RETURN VALUE 	| ERROR NUMBER 	| OTHER DATA 	| 

TOTAL LENGTH (4 bytes): This fields works that same as message from stubs to server.

*RETURN VALUE*: Return values that server will pass back to clients. It is the return value of function that server get after calling corresponding function. Its size may vary. It depends on which function is called.

*ERROR NUMBER*: This field contains value of errno, this field is set when error happens. Stubs function should set errno to ERROR NUMBER when error happens. This make stubs transparent to client.

*OTHER DATA*: This field contains data that will be used by stubs function. Some functions will designate data to pointers in its arguments. For example, read() function need to put data read from file into a buffer that create by client. Client only passes a pointer to stubs function. It is stubs’ duty to put data into that buffer. And those data will be created by server and put in this field.