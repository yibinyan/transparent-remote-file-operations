#ifndef _SOCKET_HELPER_H_
#define _SOCKET_HELPER_H_

void send_message(int sockfd, char *data, int length);
int receive_message(int sockfd, char **buf);
void free_recv_buf(char *recvbuf);

#endif
