#define _GNU_SOURCE

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h> 

#include <string.h>
#include <sys/types.h>
//#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "myheader.h"
#include <errno.h>
#include <dirtree.h>
#include "queue.h"
#include "socket_helper.h"

int valid_fd[100];
int fd_num = 0;

// The following line declares a function pointer with the same prototype as the open function.  
int (*orig_open)(const char *pathname, int flags, ...);  // mode_t mode is needed when flags includes O_CREAT
// function pointer with the same prototype as the close function
int (*orig_close)(int fd); 
// function pointer with the same prototype as the read function
ssize_t (*orig_read)(int fd, void *buf, size_t count); 
// function pointer with the same prototype as the write function
ssize_t (*orig_write)(int fd, void *buf, size_t count); 
// function pointer with the same prototype as the lssek function
off_t (*orig_lseek)(int fd, off_t offset, int whence); 
// function pointer with the same prototype as the stat function
int (*orig_xstat)(int ver, char *path, struct stat *stat_buf);
// function pointer with the same prototype as the unlink function
int (*orig_unlink)(const char *pathname);
// function pointer with the same prototype as the getdirentries function
int (*orig_getdirentries)(int fd, char *buf, int nbytes, long *basep);
// function pointer with the same prototype as the getdirtree function
struct dirtreenode* (*orig_getdirtree)( const char *path );
void (*orig_freedirtree)(struct dirtreenode* dt);
void get_full_nodes(struct dirtreenode **, struct dirtreenode *, char *);
/* check whether fd is open by server  */
int is_fd_valid(int fd);
/* remove invalid fd */
int remove_from_validfd(int t);


int create_socket(){
	char *serverip;
	char *serverport;
	unsigned short port;
	int sockfd, rv;
	struct sockaddr_in srv;
	
	// Get environment variable indicating the ip address of the server
	serverip = getenv("server15440");
	if (!serverip){
		serverip = "127.0.0.1";
    }
	
	// Get environment variable indicating the port of the server
	serverport = getenv("serverport15440");
	if (serverport) {
	    port = (unsigned short)atoi(serverport);
    } else {
		port = 15440;
	}
	
	// Create socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);	// TCP/IP socket
	if (sockfd<0) err(1, 0);			// in case of error
	
	// setup address structure to point to server
	memset(&srv, 0, sizeof(srv));			// clear it first
	srv.sin_family = AF_INET;			// IP family
	srv.sin_addr.s_addr = inet_addr(serverip);	// IP address of server
	srv.sin_port = htons(port);			// server port

	// actually connect to the server
	rv = connect(sockfd, (struct sockaddr*)&srv, sizeof(struct sockaddr));
	if (rv<0) err(1,0);
   
    return sockfd;
}

// This is our replacement for the open function from libc.
int open(const char *pathname, int flags, ...) {
	char *send_buf;
    char *current; /* current position of buf */
    char *result;
    int path_len = strlen(pathname) + 1;    
    int total_len;
    mode_t m=0;
    int fd;	

    if (flags & O_CREAT) {
		va_list a;
		va_start(a, flags);
		m = va_arg(a, mode_t);
		va_end(a);
	}
   
    /* size of mode_t equals size of integer  */
    total_len = OPERATE_TYPE_SIZE + INT_SIZE + INT_SIZE 
                                + path_len + INT_SIZE + sizeof(mode_t);
    
    send_buf = (char *)malloc(total_len); 
    current = send_buf;

    /* put total length into send buffer */
    memcpy(current, &total_len, INT_SIZE);
    /* put OPERATE TYPE into buffer */
    current = current + INT_SIZE;
    send_buf[INT_SIZE] = TYPE_OPEN;
    /* put length of path into send buffer */
    current = current + OPERATE_TYPE_SIZE;
    memcpy(current, &path_len, INT_SIZE);
    /* put pathname into send buffer */
    current = current + INT_SIZE;
    memcpy(current, pathname, path_len);
    /* put flag into send buffer */
    current = current + path_len;
    memcpy(current, &flags, INT_SIZE);
    /* put mode into send buffer */
    current = current + INT_SIZE;
    memcpy(current, &m, sizeof(mode_t));
    
    // send message to server
    int sockfd = create_socket();
    send_message(sockfd, send_buf, total_len); 
    
    receive_message(sockfd, &result);
    
    fd = *(int *)result;    

    if (fd < 0){
        errno = *(int *)(result + INT_SIZE);
    }else{
        valid_fd[fd_num] = fd;
        fd_num++;
    }
    
    free(send_buf);
    free_recv_buf(result);
    orig_close(sockfd);

    return fd;
}


int close(int fd){

    if (!is_fd_valid(fd)){
        return orig_close(fd);
    }
       
    char *send_buf;
    char *current; /* current position of buf */
    char *result;
    int total_len;
 
    total_len = INT_SIZE + OPERATE_TYPE_SIZE + INT_SIZE;
    send_buf = (char *)malloc(total_len);
    current = send_buf;        

    /* put total length into send buffer */
    memcpy(current, &total_len, INT_SIZE);
    /* put OPERATE TYPE into buffer */
    current = current + INT_SIZE;
    send_buf[INT_SIZE] = TYPE_CLOSE;
    /* put fd into send buffer */
    current = current + OPERATE_TYPE_SIZE;
    memcpy(current, &fd, INT_SIZE);    
 
    int sockfd = create_socket();
    send_message(sockfd, send_buf, total_len); 
    receive_message(sockfd, &result);
    int rv = *(int *)result;   
 
    if (rv < 0){
        errno = *(int *)(result + INT_SIZE);
    }
    
    free(send_buf);
    free_recv_buf(result);
    orig_close(sockfd);
    remove_from_validfd(fd);
    return rv;
}

ssize_t read(int fd, void *buf, size_t count){
    
    if (!is_fd_valid(fd)){
        return orig_read(fd, buf, count);
    }
    
    char *send_buf;
    char *current; /* current position of buf */
    char *result;
    int total_len;
    ssize_t size;

    total_len = INT_SIZE + OPERATE_TYPE_SIZE + 
                            INT_SIZE + sizeof(size_t);

    send_buf = (char *)malloc(total_len); 
    current = send_buf;

    /* put total length into send buffer */
    memcpy(current, &total_len, INT_SIZE);
    /* put OPERATE TYPE into buffer */
    current = current + INT_SIZE;
    send_buf[INT_SIZE] = TYPE_READ;
    /* put fd into send buffer */
    current = current + OPERATE_TYPE_SIZE;
    memcpy(current, &fd, INT_SIZE);
    /* put count into send buffer */ 
    current = current + INT_SIZE;
    memcpy(current, &count, sizeof(size_t));

    /* send message to server */
    int sockfd = create_socket();
    send_message(sockfd, send_buf, total_len); 
    receive_message(sockfd, &result);
    size = *(ssize_t *)result;    

    if (size < 0){
        errno = *(int *)(result + sizeof(ssize_t));
    }else{
        memcpy(buf, result + sizeof(ssize_t) + INT_SIZE, size);
    }
    
    
    free(send_buf);    
    free_recv_buf(result);
    orig_close(sockfd);

    return size;
}

ssize_t write(int fd, void *buf, size_t count){
    
    if (!is_fd_valid(fd)){
        return orig_write(fd, buf, count);
    }
    
    char *send_buf;
    char *current; /* current position of buf */
    char *result;
    int total_len;
    ssize_t size;

    total_len = INT_SIZE + OPERATE_TYPE_SIZE + 
                            INT_SIZE + sizeof(size_t) + count;

    send_buf = (char *)malloc(total_len); 
    current = send_buf;

    /* put total length into send buffer */
    memcpy(current, &total_len, INT_SIZE);
    /* put OPERATE TYPE into buffer */
    current = current + INT_SIZE;
    send_buf[INT_SIZE] = TYPE_WRITE;
    /* put fd into send buffer */
    current = current + OPERATE_TYPE_SIZE;
    memcpy(current, &fd, INT_SIZE);
    /* put size of content into send buffer */ 
    current = current + INT_SIZE;
    memcpy(current, &count, sizeof(size_t));
    /* put content into send buffer */
    current = current + sizeof(size_t);
    memcpy(current, buf, count);

    /* send message to server */
    int sockfd = create_socket();
    send_message(sockfd, send_buf, total_len); 
    receive_message(sockfd, &result);
    size = *(ssize_t *)result;    

    if (size < 0){
        errno = *(int *)(result + sizeof(ssize_t));
    }

    free(send_buf);    
    free_recv_buf(result);
    orig_close(sockfd);

    return size;
}

off_t lseek(int fd, off_t offset, int whence){
    
    if (!is_fd_valid(fd)){
        return orig_lseek(fd, offset, whence);
    }
    

    char *send_buf;
    char *current; /* current position of buf */
    char *result;
    int total_len;
    off_t location;

    total_len = INT_SIZE + OPERATE_TYPE_SIZE + 
                        INT_SIZE + sizeof(off_t) + INT_SIZE;

    send_buf = (char *)malloc(total_len); 
    current = send_buf;

    /* put total length into send buffer */
    memcpy(current, &total_len, INT_SIZE);
    /* put OPERATE TYPE into buffer */
    current = current + INT_SIZE;
    send_buf[INT_SIZE] = TYPE_LSEEK;
    /* put fd into send buffer */
    current = current + OPERATE_TYPE_SIZE;
    memcpy(current, &fd, INT_SIZE);
    /* put size of offset into send buffer */ 
    current = current + INT_SIZE;
    memcpy(current, &offset, sizeof(off_t));
    /* put whence into send buffer */
    current = current + sizeof(off_t);
    memcpy(current, &whence, INT_SIZE);

    /* send message to server */
    int sockfd = create_socket();
    send_message(sockfd, send_buf, total_len); 
    receive_message(sockfd, &result);
    location = *(off_t *)result;    
    
    if (location < 0){
        errno = *(int *)(result + sizeof(off_t));
    }
    
    free(send_buf);
    free_recv_buf(result);
    orig_close(sockfd);

    return location;
}

int __xstat(int ver, char *path, struct stat *stat_buf){
    char *send_buf;
    char *current; /* current position of buf */
    char *result;
    int path_len = strlen(path) + 1;    
    int total_len;
    int rv;

    total_len = INT_SIZE + OPERATE_TYPE_SIZE + 
                        INT_SIZE + INT_SIZE + path_len;

    send_buf = (char *)malloc(total_len); 
    current = send_buf;

    /* put total length into send buffer */
    memcpy(current, &total_len, INT_SIZE);
    /* put OPERATE TYPE into buffer */
    current = current + INT_SIZE;
    send_buf[INT_SIZE] = TYPE_XSTAT;
    /* put version into send buffer */
    current = current + OPERATE_TYPE_SIZE;
    memcpy(current, &ver, INT_SIZE);
    /* put length of path into send buffer */
    current = current + INT_SIZE;
    memcpy(current, &path_len, INT_SIZE);
    /* put path into send buffer */ 
    current = current + INT_SIZE;
    memcpy(current, path, path_len);

    /* send message to server */
    int sockfd = create_socket();
    send_message(sockfd, send_buf, total_len); 
    
    receive_message(sockfd, &result);
    rv = *(int *)result;    
    
    if (rv < 0){
        errno = *(int *)(result + INT_SIZE);
//printf("XSTAT ERROR\n");
    }else{
        memcpy(stat_buf, result + INT_SIZE + INT_SIZE, sizeof(struct stat));
    }
    
    free(send_buf);
    free_recv_buf(result);
    orig_close(sockfd);

    return rv;
}

int unlink(const char *pathname){
	char *send_buf;
    char *current; /* current position of buf */
    char *result;
    int path_len = strlen(pathname) + 1;    
    int total_len;
    int rv;	

    /* size of mode_t equals size of integer  */
    total_len = INT_SIZE + OPERATE_TYPE_SIZE + INT_SIZE + path_len;
    
    send_buf = (char *)malloc(total_len); 
    current = send_buf;

    /* put total length into send buffer */
    memcpy(current, &total_len, INT_SIZE);
    /* put OPERATE TYPE into buffer */
    current = current + INT_SIZE;
    send_buf[INT_SIZE] = TYPE_UNLINK;
    /* put length of path into send buffer */
    current = current + OPERATE_TYPE_SIZE;
    memcpy(current, &path_len, INT_SIZE);
    /* put pathname into send buffer */
    current = current + INT_SIZE;
    memcpy(current, pathname, path_len);

    /* send message to server */
    int sockfd = create_socket();
    send_message(sockfd, send_buf, total_len); 
    receive_message(sockfd, &result);
    
    rv = *(int *)result;
    
    if (rv < 0){
        //printf("ULINK ERROR\n");
        errno = *(int *)(result + INT_SIZE);
    }

    free(send_buf);
    free_recv_buf(result);
    orig_close(sockfd);

    return rv;
}

ssize_t getdirentries(int fd, char *buf, int nbytes, off_t *basep){
    
    if (!is_fd_valid(fd)){
        return orig_getdirentries(fd, buf, nbytes, basep);
    }
    

    char *send_buf;
    char *current; /* current position of buf */
    char *result;
    int total_len;
    ssize_t rv;

    total_len = INT_SIZE + OPERATE_TYPE_SIZE + 
                        INT_SIZE + INT_SIZE + sizeof(off_t);

    send_buf = (char *)malloc(total_len); 
    current = send_buf;

    /* put total length into send buffer */
    memcpy(current, &total_len, INT_SIZE);
    /* put OPERATE TYPE into buffer */
    current = current + INT_SIZE;
    send_buf[INT_SIZE] = TYPE_GETDIRENTRIES;
    /* put fd into send buffer */
    current = current + OPERATE_TYPE_SIZE;
    memcpy(current, &fd, INT_SIZE);
    /* put nbytes into send buffer */
    current = current + INT_SIZE;
    memcpy(current, &nbytes, INT_SIZE);
    /* put size of offset into send buffer */ 
    current = current + INT_SIZE;
    memcpy(current, basep, sizeof(off_t));

    /* send message to server */
    int sockfd = create_socket();
    send_message(sockfd, send_buf, total_len); 
    
    receive_message(sockfd, &result);
    rv = *(ssize_t *)result;    

    if (rv < 0){
        errno = *(int *)(result + sizeof(ssize_t));
    }else{
        memcpy(buf, result + sizeof(ssize_t) + INT_SIZE, rv);
    }
    
    free(send_buf);
    free_recv_buf(result);
    orig_close(sockfd);
    return rv;
}

struct dirtreenode* getdirtree(const char *path){
    char *send_buf;
    char *current; /* current position of buf */
    char *result;
    char *name_buffer; 
    int path_len = strlen(path) + 1;    
    int total_len;
    int rv;
    struct dirtreenode* root = NULL; 

    /* size of mode_t equals size of integer  */
    total_len = INT_SIZE + OPERATE_TYPE_SIZE + INT_SIZE + path_len;
    
    send_buf = (char *)malloc(total_len); 
    current = send_buf;

    /* put total length into send buffer */
    memcpy(current, &total_len, INT_SIZE);
    /* put OPERATE TYPE into buffer */
    current = current + INT_SIZE;
    send_buf[INT_SIZE] = TYPE_GETDIRTREE;
    /* put length of path into send buffer */
    current = current + OPERATE_TYPE_SIZE;
    memcpy(current, &path_len, INT_SIZE);
    /* put pathname into send buffer */
    current = current + INT_SIZE;
    memcpy(current, path, path_len);

    /* send message to server */
    int sockfd = create_socket();
    send_message(sockfd, send_buf, total_len); 
    
    /* first receive all the struct */
    receive_message(sockfd, &result);

    rv = *(int *)result;    

//printf("Start creating tree\n");

    if (rv == 0){
        /* return null */
        root = NULL;
        errno = *(int *)(result + INT_SIZE);
        free_recv_buf(result);
    }else{
        char *nodes_buffer = result + INT_SIZE + INT_SIZE;
       
        int ask_for_name = 1;
        send(sockfd, &ask_for_name, INT_SIZE, 0);  
        receive_message(sockfd, &name_buffer);
      
        get_full_nodes(&root, (struct dirtreenode *)nodes_buffer, name_buffer);
    }
    
    
    free(send_buf);
    //free_recv_buf(result);
    //free_recv_buf(name_buffer);
    //memcpy(rv, result.name, )
    orig_close(sockfd);

    return root;
   // return orig_getdirtree(path);
}

void get_full_nodes(struct dirtreenode **root,
                                    struct dirtreenode *nodes_buffer,
                                        char *name_buffer){

    struct dirtreenode *current 
                = nodes_buffer;
    
    nodes_buffer = nodes_buffer + 1;

    int current_name_size = strlen(name_buffer) + 1;
    char *current_name = name_buffer;
    current->name = current_name;
    name_buffer = name_buffer + current_name_size; 
    
    queue_t node_queue;
    queue_init(&node_queue);
    enqueue(&node_queue, (void *)current);    

    while (!is_queue_empty(&node_queue)){
        int i;
        struct dirtreenode *parent = (struct dirtreenode *)dequeue(&node_queue);        

        parent->subdirs 
            = (struct dirtreenode **)malloc(sizeof(struct dirtreenode *) 
                                                          * parent->num_subdirs);
        for (i = 0; i < parent->num_subdirs; i++){
            struct dirtreenode *child = nodes_buffer + i;
            
            int name_size = strlen(name_buffer) + 1;

            child->name = name_buffer;
            name_buffer = name_buffer + name_size;
            parent->subdirs[i] = child;

            enqueue(&node_queue, (void *)child);
        }

        nodes_buffer = nodes_buffer + i;
    }


    queue_destroy(&node_queue);    
    
    *root = current;
}

void freedirtree( struct dirtreenode* dt){
    
    queue_t node_queue;
    queue_init(&node_queue);

    enqueue(&node_queue, (void *)dt);

    while (!is_queue_empty(&node_queue)){
        struct dirtreenode *node = (struct dirtreenode *)dequeue(&node_queue); 
        
        int i = 0;
        for (i = 0; i < node->num_subdirs; i++){
            enqueue(&node_queue, node->subdirs[i]);
        }
        
        //free(node->name);
        free(node->subdirs);
        //free(node);

    }

    free(dt->name - 4);
    free((char *)dt - 12);
    queue_destroy(&node_queue);
    dt = NULL;
}



// This function is automatically called when program is started
void _init(void) {
	// set function pointer orig_open to point to the original open function
	orig_open = dlsym(RTLD_NEXT, "open");
	// set function pointer orig_close to point to the original close function
    orig_close = dlsym(RTLD_NEXT, "close"); 
	// set function pointer orig_read to point to the original read function
    orig_read = dlsym(RTLD_NEXT, "read"); 
	// set function pointer orig_write to point to the original write function
    orig_write = dlsym(RTLD_NEXT, "write"); 
	// set function pointer orig_lseek to point to the original lseek function
    orig_lseek = dlsym(RTLD_NEXT, "lseek"); 
	// set function pointer orig_xstat to point to the original stat function
    orig_xstat = dlsym(RTLD_NEXT, "__xstat"); 
	// set function pointer orig_unlink to point to the original unlink function
    orig_unlink = dlsym(RTLD_NEXT, "unlink");
    orig_getdirentries = dlsym(RTLD_NEXT, "getdirentries");
	// set function pointer orig_unlink to point to the original unlink function
    orig_getdirtree = dlsym(RTLD_NEXT, "getdirtree");
    orig_freedirtree = dlsym(RTLD_NEXT, "freedirtree");
}


int is_fd_valid(int fd){
    int i;
    for (i = 0; i < fd_num; i++){
        if (fd == valid_fd[i]){
            return 1;
        } 
    }    

    return 0;
}

int remove_from_validfd(int fd){
    int i;
    for (i = 0; i < fd_num; i++){
        if (fd == valid_fd[i]){
            int j;
            for (j = i + 1; j < fd_num; j++){
                valid_fd[j-1] = valid_fd[j];
            }
            
            break;
        } 
    }    

    fd_num--;
    return 0;
}


