#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <err.h>
#include "myheader.h"
#include <errno.h>
#include <pthread.h>
#include <stdint.h>
#include <dirtree.h>
#include "queue.h"
#include "socket_helper.h"

void *request_handle_thread(void *);
void process_write_msg(int, char *);
void process_read_msg(int, char *);
void process_open_msg(int, char *);
void process_close_msg(int, char *);
void process_getdirentries_msg(int, char *);
void process_unlink_msg(int, char *);
void process_lseek_msg(int, char *);
void process_xstat_msg(int, char *);
void process_getdirtree_msg(int, char *);

int main(int argc, char**argv) {
	char *serverport;
	unsigned short port;
	int sockfd, sessfd, rv;
	struct sockaddr_in srv, cli;
	socklen_t sa_size;
	
	// Get environment variable indicating the port of the server
	serverport = getenv("serverport15440");
	if (serverport) port = (unsigned short)atoi(serverport);
	else port=15440;
	
	// Create socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);	// TCP/IP socket
	if (sockfd<0) err(1, 0);			// in case of error
	
	// setup address structure to indicate server port
	memset(&srv, 0, sizeof(srv));			// clear it first
	srv.sin_family = AF_INET;			// IP family
	srv.sin_addr.s_addr = htonl(INADDR_ANY);	// don't care IP address
	srv.sin_port = htons(port);			// server port

	// bind to our port
	rv = bind(sockfd, (struct sockaddr*)&srv, sizeof(struct sockaddr));
	if (rv<0) err(1,0);
	
	// start listening for connections, queue up to 5
	rv = listen(sockfd, 5);
	if (rv<0) err(1,0);
	
	// main server loop, handle clients one at a times
    while (1){	
		// wait for next client, get session socket
		sa_size = sizeof(struct sockaddr_in);
		sessfd = accept(sockfd, (struct sockaddr *)&cli, &sa_size);
		if (sessfd<0) err(1,0);
	
        pthread_t handler;

        /* create thread to process request */
        if (pthread_create(&handler, NULL, 
                            request_handle_thread, (void *)(intptr_t)sessfd) < 0){
            perror("Could not create thread ");
            return -1;
        }
    }

    pthread_exit(NULL);
	// close socket
	close(sockfd);

    return 0;
}

void *request_handle_thread(void *socket){
//	char buf[MAXMSGLEN]; 
    char *output;
    int sessfd = (intptr_t)socket;
    int rv = receive_message(sessfd, &output);
    char type = output[0];
    /* skip total size field and type field */
    char *request_content = output + OPERATE_TYPE_SIZE;
 
    switch (type){
        case TYPE_OPEN: process_open_msg(sessfd, request_content); break;
        case TYPE_WRITE: process_write_msg(sessfd, request_content); break;
        case TYPE_READ: process_read_msg(sessfd, request_content); break;
        case TYPE_CLOSE: process_close_msg(sessfd, request_content); break; 
        case TYPE_LSEEK: process_lseek_msg(sessfd, request_content); break;
        case TYPE_UNLINK: process_unlink_msg(sessfd, request_content); break;
        case TYPE_GETDIRENTRIES: process_getdirentries_msg(sessfd, request_content); break; 
        case TYPE_XSTAT: process_xstat_msg(sessfd, request_content); break;
        case TYPE_GETDIRTREE: process_getdirtree_msg(sessfd, request_content); break;
        default: printf("Type not found\n"); 
    } 

    /* either client closed connection, or error */
    if (rv<0) err(1,0);
    free_recv_buf(output);
    close(sessfd);

    return 0;
}


void process_open_msg(int sessfd, char *buf){
    int path_len = *(int *)(buf);
    int flag = *(int *)(buf + INT_SIZE + path_len);
    mode_t m = *(mode_t *)(buf + INT_SIZE + path_len + INT_SIZE);
    char *path = (char *)malloc(path_len);
    char send_buf[20];
    
    memcpy(path, buf + INT_SIZE, path_len);
   

printf("Filename: %s\n", path);
printf("Flag: %d\n", flag);
printf("Mode: %d\n", m);

 
    int fd = open(path, flag, m);

    /* total length of send buffer */
    int total_len = INT_SIZE + INT_SIZE + INT_SIZE;
    /* set send buffer */
    memcpy(send_buf, &total_len, INT_SIZE);
    memcpy(send_buf + INT_SIZE, &fd, INT_SIZE);
    memcpy(send_buf + INT_SIZE + INT_SIZE, &errno, INT_SIZE);

printf("fd = %d, OPEN DONE!!\n", fd);
    send_message(sessfd, send_buf, total_len);   
    
    free(path);
}



void process_write_msg(int sessfd, char *buf){
    int fd = *(int *)(buf);
    size_t count = *(size_t *)(buf + INT_SIZE);
    printf("fd = %d, count = %ld WRITing!\n", fd, count);
    char *content = (char *)malloc(count);
    char send_buf[20]; 

    memcpy(content, buf + INT_SIZE + sizeof(size_t), count);


    
    ssize_t rv = write(fd, content, count);
    
    printf("fd = %d, count = %ld, result = %ld, WRITE DONE!\n", fd, count, rv);
    
    /* total length of send buffer */
    int total_len = INT_SIZE + sizeof(ssize_t) + INT_SIZE;
    /* set send buffer */
    memcpy(send_buf, &total_len, INT_SIZE);
    memcpy(send_buf + INT_SIZE, &rv, sizeof(ssize_t));
    memcpy(send_buf + INT_SIZE + sizeof(ssize_t), &errno, INT_SIZE);
    send_message(sessfd, send_buf, total_len);

    free(content);
}

void process_read_msg(int sessfd, char *buf){
    int fd = *(int *)(buf);
    size_t count = *(size_t *)(buf + INT_SIZE);
    char *content = (char *)malloc(count);
    
    ssize_t rv = read(fd, content, count);
    
    char *send_buf;
    /* total length of send buffer */
    int total_len;

    if (rv < 0){
        total_len = INT_SIZE + sizeof(ssize_t) + INT_SIZE;
    }else {
        total_len = INT_SIZE + sizeof(ssize_t) + INT_SIZE + rv;
    }
    
    send_buf = malloc(total_len); 

    /* set send buffer */
    memcpy(send_buf, &total_len, INT_SIZE);
    memcpy(send_buf + INT_SIZE, &rv, sizeof(ssize_t));
    memcpy(send_buf + INT_SIZE + sizeof(ssize_t), &errno, INT_SIZE);
    if (rv >= 0){
        memcpy(send_buf + INT_SIZE + sizeof(ssize_t) + INT_SIZE, content, rv);
    }

    send_message(sessfd, send_buf, total_len);

    free(send_buf);
    free(content);
    printf("READ DONE");
    //printf("fd = %d, count = %ld, result = %ld, READ DONE!\n", fd, count, rv);
}


void process_close_msg(int sessfd, char *buf){
    int fd = *(int *)(buf);
    char send_buf[20]; 
    int rv = close(fd);
    
    printf("fd = %d, result = %d, CLOSE DONE!\n", fd, rv);
    
    /* total length of send buffer */
    int total_len = INT_SIZE + INT_SIZE + INT_SIZE;
    /* set send buffer */
    memcpy(send_buf, &total_len, INT_SIZE);
    memcpy(send_buf + INT_SIZE, &rv, INT_SIZE);
    memcpy(send_buf + INT_SIZE + INT_SIZE, &errno, INT_SIZE);
    send_message(sessfd, send_buf, total_len);
}

void process_unlink_msg(int sessfd, char *buf){
    int path_len = *(int *)(buf);    
    char send_buf[20];
    char *path = (char *)malloc(path_len);
    memcpy(path, buf + INT_SIZE, path_len);

    int rv = unlink(path);

    /* total length of send buffer */
    int total_len = INT_SIZE + INT_SIZE + INT_SIZE;
    /* set send buffer */
    memcpy(send_buf, &total_len, INT_SIZE);
    memcpy(send_buf + INT_SIZE, &rv, INT_SIZE);
    memcpy(send_buf + INT_SIZE + INT_SIZE, &errno, INT_SIZE);
    
//printf("unlink error: %d", errno);
    send(sessfd, send_buf, total_len, 0);

    printf("UNLINK DONE!\n");
    free(path);
}

void process_getdirentries_msg(int sessfd, char *buf){
    int fd = *(int *)(buf);    
    char *send_buf;
    int nbytes = *(int *)(buf + INT_SIZE);
    off_t basep = *(off_t *)(buf + INT_SIZE + INT_SIZE);
    char *dirent_buf = (char *)malloc(nbytes);

    ssize_t rv = getdirentries(fd, dirent_buf, nbytes, &basep);

printf("fd = %d, nbytes = %d", fd, nbytes);
   
    /* total length of send buffer */
    int total_len = INT_SIZE + sizeof(ssize_t) + INT_SIZE + nbytes;
    send_buf = (char *)malloc(total_len);
    
     /* set send buffer */
    memcpy(send_buf, &total_len, INT_SIZE);
    memcpy(send_buf + INT_SIZE, &rv, sizeof(ssize_t));
    memcpy(send_buf + INT_SIZE + sizeof(ssize_t), &errno, INT_SIZE);
   
    int current_len = INT_SIZE + sizeof(ssize_t) + INT_SIZE;
    //int rest_len = nbytes;
    memcpy(send_buf + current_len, dirent_buf, nbytes);

    send(sessfd, send_buf, total_len, 0);
    free(dirent_buf);
    free(send_buf);
    printf("GETDIRENTRIES DONE!\n");
}

void process_lseek_msg(int sessfd, char *buf){
    int fd = *(int *)(buf);    
    off_t offset = *(off_t *)(buf + INT_SIZE);
    int whence = *(int *)(buf + INT_SIZE + sizeof(off_t));
    char send_buf[20];

    off_t rv = lseek(fd, offset, whence);
   
    /* total length of send buffer */
    int total_len = INT_SIZE + sizeof(off_t) + INT_SIZE;
    /* set send buffer */
    memcpy(send_buf, &total_len, INT_SIZE);
    memcpy(send_buf + INT_SIZE, &rv, sizeof(off_t));
    memcpy(send_buf + INT_SIZE + sizeof(off_t), &errno, INT_SIZE);
    
    send_message(sessfd, send_buf, total_len);
    printf("LSEEK DONE!\n");
}


void process_xstat_msg(int sessfd, char *buf){
    int ver = *(int *)(buf);    
    int path_len = *(int *)(buf + INT_SIZE);
    char *path  = (char *)malloc(path_len); 
    char *send_buf;
    int rv;
    //struct stat *stat_buf = (struct stat *)malloc(sizeof(struct stat));
    struct stat stat_buf;

    memcpy(path, buf + INT_SIZE + INT_SIZE, path_len);
    rv = __xstat(ver, path, &stat_buf);

    /* total length of send buffer */
    int total_len = INT_SIZE + INT_SIZE + INT_SIZE + sizeof(struct stat);
    send_buf = (char *)malloc(total_len);
    /* set send buffer */
    memcpy(send_buf, &total_len, INT_SIZE);
    memcpy(send_buf + INT_SIZE, &rv, INT_SIZE);
    memcpy(send_buf + INT_SIZE + INT_SIZE, &errno, INT_SIZE);
    memcpy(send_buf + INT_SIZE + INT_SIZE + INT_SIZE, 
                                        &stat_buf, sizeof(struct stat));   
 
    send_message(sessfd, send_buf, total_len);
    free(path);
    //free(stat_buf);
    free(send_buf);
    printf("XSTAT DONE!\n");
}

void process_getdirtree_msg(int sessfd, char *buf){
    int path_len = *(int *)(buf);
    char *path  = (char *)malloc(path_len); 
    char *send_buf = (char *)malloc(MAXMSGLEN);
    int send_buf_max = MAXMSGLEN;
    char *temp_name_buf = (char *)malloc(MAXMSGLEN);
    int temp_buf_max = MAXMSGLEN;
    char *name_buf;
    int flag = 0; /* flag indicate  */
    struct dirtreenode *dirtree;
    queue_t node_queue;
    
    memcpy(path, buf + INT_SIZE, path_len);
    dirtree = getdirtree(path);

    /* total length of send buffer and name buffer */
    int sendbuf_len = INT_SIZE + INT_SIZE + INT_SIZE;
    int namebuf_len = 0;
    
    if (dirtree != NULL){
        flag = 1; /* 1 means find subdir */
        queue_init(&node_queue);
        enqueue(&node_queue, (void *)dirtree);
    
        while (!is_queue_empty(&node_queue)){
            struct dirtreenode *node1 
                        = (struct dirtreenode *)dequeue(&node_queue);
            
            struct dirtreenode node;
            memset(&node, 0, sizeof(struct dirtreenode));
            node.name = node1->name;
            node.num_subdirs = node1->num_subdirs;

            /* put content in to send buffer */
            if (sendbuf_len + sizeof(struct dirtreenode) > send_buf_max){
                send_buf_max = send_buf_max + MAXMSGLEN;
                char *new_send_buf;
                new_send_buf = realloc(send_buf, send_buf_max);
                send_buf = new_send_buf;
            }
            memcpy(send_buf + sendbuf_len, &node, sizeof(struct dirtreenode));
            sendbuf_len = sendbuf_len + sizeof(struct dirtreenode); 
            
            /* put name string into name buffer */
            int tmp_len = strlen(node1->name) + 1;
            if (namebuf_len + tmp_len > temp_buf_max){
                temp_buf_max = temp_buf_max + MAXMSGLEN;
                char *new_tmp_buf;
                new_tmp_buf = realloc(temp_name_buf, temp_buf_max);
                temp_name_buf = new_tmp_buf;
            }
            memcpy(temp_name_buf + namebuf_len, node1->name, tmp_len);    
            namebuf_len = namebuf_len + tmp_len;            
            
//printf("server enqueue name : %s\n", node->name);

            int i;
            for (i = 0; i < node1->num_subdirs; i++){
                enqueue(&node_queue, (void *)node1->subdirs[i]);
            }              
        }
    }
   
    queue_destroy(&node_queue);
     
    /* set send buffer */
    memcpy(send_buf, &sendbuf_len, INT_SIZE);
    memcpy(send_buf + INT_SIZE, &flag, INT_SIZE);
    memcpy(send_buf + INT_SIZE + INT_SIZE, &errno, INT_SIZE);
 
    if (dirtree != NULL){
        send_message(sessfd, send_buf, sendbuf_len);
        
        int ask_for_name = 0;
        recv(sessfd, &ask_for_name, INT_SIZE, 0);
        
        if (ask_for_name){
            name_buf = (char *)malloc(namebuf_len + INT_SIZE);
            
            memcpy(name_buf + INT_SIZE, temp_name_buf, namebuf_len);
            namebuf_len = namebuf_len + INT_SIZE;
            memcpy(name_buf, &namebuf_len, INT_SIZE);
            
            send_message(sessfd, name_buf, namebuf_len);   
            //printf("server: NAME BUF SIZE: %d, name size %d, %s|", 
            //                *(int *)name_buf, namebuf_len, name_buf+4);
            free(name_buf);
        }
    }else{
        /* can't find subdir */
        send_message(sessfd, send_buf, sendbuf_len);
    }

    free(path);
    free(send_buf);
    free(temp_name_buf);
    freedirtree(dirtree);
 
   // printf("DIRTREE DONE!\n");
}

